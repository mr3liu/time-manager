#Name: Time Manager
#################################################################ON already running: END ALL APPS!
#sort in time order
#add and edit validation
#close all apps in already running message and alarm message
#size of app and messages
#if font becomes "." font=datfont, need redefine?
#
from tkinter import *
from tkinter.ttk import *
from tkinter import font
#from tkinter import messagebox
import calendar
import datetime
import pickle



class TimeManager(Tk):
    """..."""

    def __init__(self, error, data=None):
        """..."""

        super().__init__()
        #self.title("Time Manager")
        self.resizable(False, False)
        #self.attributes("-topmost", True) #no

        self.cou12Font = font.Font(family="Courier", size=12)
        self.cou16Font = font.Font(family="Courier", size=16, weight="bold")
        print(font.families())
        self.style = Style()
        self.style.theme_use("alt") #winnative?
        self.style.configure("Container.TFrame", background="red")
        #self.cou12Font = font.Font(family="Courier", size=12)
        self.style.configure("Norm.TButton", background="white", font=self.cou12Font)
        self.style.map("Norm.TButton", background=[("pressed", "red3"), ("active", "palevioletred1")]) #mightn't need pressed
        self.style.configure("Nav.Norm.TButton", width=2)
        #self.cou16Font = font.Font(family="Courier", size=16, weight="bold")
        self.style.configure("MthAndYr.TLabel", background="red", font=self.cou16Font, foreground="white")
        self.style.configure("WeekHeader.TLabel", anchor=E, background="lightpink", font=self.cou12Font, padding=1)
        self.style.configure("Day.TButton", anchor=E, background="white", font=self.cou12Font, relief=FLAT, width=3)
        self.style.map("Day.TButton", background=[("pressed", "red3"), ("active", "palevioletred1")], relief=[("pressed", "flat"), ("active", "flat")]) #mightn't need pressed
        self.style.configure("Sel.Day.TButton", background="red")
        self.style.map("Blank.Day.TButton", background=[("pressed", "white"), ("active", "white")]) #mightn't need pressed
        self.style.configure("SelDate.TLabel", background="white", font=self.cou16Font)
        self.style.configure("Evt.Treeview", font=self.cou12Font, relief=FLAT)
        self.style.configure("Mess.TLabel", background="white", font=self.cou12Font)

        if error:
            self.title("Error")
            self.attributes("-topmost", True)
            self.mainFrame = Frame(self, padding=20, style="Container.TFrame")
            self.mainFrame.pack()
            self.messLabel = Label(self.mainFrame, style="Mess.TLabel", text="This application is already running.")
            self.messLabel.grid(row=0, column=0, padx=5, pady=5)
            self.okButton = Button(self.mainFrame, style="Norm.TButton", command=self.destroy, text="OK")
            self.okButton.grid(row=1, column=0, padx=5, pady=(20, 5))
            return

        self.title("Time Manager")

        self.data = data

        self.caldate = datetime.date.today().replace(day=1)
        self.seldate = datetime.date.today()

        #with open("time_manager_data", "rb") as file:
        #    self.data = pickle.load(file)
        
        self.create_widgets()

        def destroy():
            self.data["running"] = False
            with open("time_manager_data", "wb") as file:
                pickle.dump(self.data, file)
            self.destroy()
        self.protocol("WM_DELETE_WINDOW", destroy)

        self.check_alarm()

    def create_widgets(self):
        """..."""

        self.mainFrame = Frame(self)
        self.mainFrame.pack() #padx=5, pady=5?
        self.codeString = StringVar(value="Type code here")
        self.codeEntry = Entry(self.mainFrame, textvariable=self.codeString)
        self.codeEntry.grid(row=2, column=0, padx=5, pady=5, sticky=EW)
        self.codeEntry.bind("<KeyPress-Return>", self.codeCommand)
        self.codeButton = Button(self.mainFrame, command=self.codeCommand, text="Execute Code")
        self.codeButton.grid(row=3, column=0, padx=5, pady=5, sticky=EW)
        
####################################################################################MAKE CALENDAR AND FONTS LARGER, CONFIG R&C, CHANGE ANCHOR!, change "for THISNAME in range(n):"
        
##        self.style.configure("Container.TFrame", background="red")
        self.calFrame = Frame(self.mainFrame, style="Container.TFrame")
        self.calFrame.grid(row=0, column=0, padx=5, pady=5)
##        self.cou12Font = font.Font(family="Courier", size=12)
##        self.style.configure("Norm.TButton", background="white", font=self.cou12Font)
##        self.style.map("Norm.TButton", background=[("pressed", "red3"), ("active", "palevioletred1")]) #mightn't need pressed
##        self.style.configure("Nav.Norm.TButton", width=2)
        self.prevnavButton = Button(self.calFrame, command=self.prevnavButtonCommand, style="Nav.Norm.TButton", text="◄")
        self.prevnavButton.grid(row=0, column=0, padx=5, pady=5)
##        self.cou16Font = font.Font(family="Courier", size=16, weight="bold")
##        self.style.configure("MthAndYr.TLabel", background="red", font=self.cou16Font, foreground="white")
        self.calFrame.columnconfigure(1, weight=1)
        self.mthandyrLabel = Label(self.calFrame, style="MthAndYr.TLabel", text=self.caldate.strftime("%B %Y"))
        self.mthandyrLabel.grid(row=0, column=1, padx=5, pady=5)
        self.nextnavButton = Button(self.calFrame, command=self.nextnavButtonCommand, style="Nav.Norm.TButton", text="►")
        self.nextnavButton.grid(row=0, column=2, padx=5, pady=5)
        self.wkhdanddayFrame = Frame(self.calFrame, style="Container.TFrame")
        self.wkhdanddayFrame.grid(row=1, column=0, columnspan=3, padx=5, pady=(0, 5))
        self.weekheaderLabels = {}
##        self.style.configure("WeekHeader.TLabel", anchor=E, background="lightpink", font=self.cou12Font, padding=1)
        calendar.setfirstweekday(6)
        weekheaders = calendar.weekheader(3).split()
        for i in range(7):
            self.weekheaderLabels[i] = Label(self.wkhdanddayFrame, style="WeekHeader.TLabel", text=weekheaders[i])
            self.weekheaderLabels[i].grid(row=0, column=i, padx=1, pady=1, sticky=NSEW)
        self.dayandbldayButtons = {}
##        self.style.configure("Day.TButton", anchor=E, background="white", font=self.cou12Font, relief=FLAT, width=3)
##        self.style.map("Day.TButton", background=[("pressed", "red3"), ("active", "palevioletred1")], relief=[("pressed", "flat"), ("active", "flat")]) #mightn't need pressed
##        self.style.configure("Sel.Day.TButton", background="red")
##        self.style.map("Blank.Day.TButton", background=[("pressed", "white"), ("active", "white")]) #mightn't need pressed
        weeks = calendar.monthcalendar(self.caldate.year, self.caldate.month)
        while len(weeks) < 6:
            weeks.append([0] * 7)
        for r in range(len(weeks)):
            for c in range(7):
                if weeks[r][c] != 0:
                    def create_dayButton(r, c):
                        self.dayandbldayButtons[(r, c)] = Button(self.wkhdanddayFrame, command=lambda: self.dayButtonCommand(r, c, weeks[r][c]), style="Day.TButton", text=str(weeks[r][c]))
                        if weeks[r][c] == self.seldate.day:
                            self.seldaybuttonpos = (r, c)
                            self.dayandbldayButtons[(r, c)].configure(style="Sel.Day.TButton")
                    create_dayButton(r, c)
                else:
                    self.dayandbldayButtons[(r, c)] = Button(self.wkhdanddayFrame, style="Blank.Day.TButton", takefocus=False)
                self.dayandbldayButtons[(r, c)].grid(row=r + 1, column=c, padx=1, pady=1, sticky=NSEW)
                
#####################################################################################################Event frame
        self.evtFrame = Frame(self.mainFrame, style="Container.TFrame")
        self.evtFrame.grid(row=0, column=1, padx=5, pady=5, sticky=NS)
##        self.style.configure("SelDate.TLabel", background="white", font=self.cou16Font)
        self.seldateLabel = Label(self.evtFrame, style="SelDate.TLabel", text=self.seldate.strftime("%A, %d %B, %Y"))
        self.seldateLabel.grid(row=0, column=0, columnspan=5, padx=5, pady=5)
##        self.style.configure("Evt.Treeview", font=self.cou12Font, relief=FLAT)
        cols = ("Time", "Event", "Alarm")
        self.evtTreeview = Treeview(self.evtFrame, columns=cols, height=11, selectmode="none", show="", style="Evt.Treeview")
        self.evtTreeview.grid(row=1, column=0, columnspan=5, padx=5, pady=5)
        self.evtTreeview.column("Time", width=60)
        self.evtTreeview.column("Event", width=380)
        self.evtTreeview.column("Alarm", width=60)
        self.evtTreeview.tag_configure("header", background="lightpink")
        self.evtTreeview.insert("", END, values=cols, tags="header")        
        self.evtiids = []
        self.evtTreeview.tag_bind("event", "<1>", self.sel_evt)
        self.evtTreeview.tag_configure("sel", background="red")
        try:
            for i in range(len(self.data[self.seldate])):
                #print(self.data[self.seldate][i])
                self.evtiids.append(self.evtTreeview.insert("", END, tags="event", values=self.data[self.seldate][i]))
        except KeyError:
            pass
        self.selevtiid = ""
        self.evtFrame.columnconfigure(1, weight=1)
        self.evtFrame.columnconfigure(3, weight=1)
        self.timeEntry = Entry(self.evtFrame, font=self.cou12Font, width=5)
        self.timeEntry.grid(row=2, column=0, padx=5, pady=5)
        self.evtEntry = Entry(self.evtFrame, font=self.cou12Font)
        self.evtEntry.grid(row=2, column=1, columnspan=3, padx=5, pady=5, sticky=EW)
        self.almEntry = Entry(self.evtFrame, font=self.cou12Font, width=5)
        self.almEntry.grid(row=2, column=4, padx=5, pady=5)
        #self.evtctrlFrame = Frame(self.evtFrame, style="Container.TFrame")
        #self.evtctrlFrame.grid(row=3, column=0, columnspan=3, padx=5, pady=5)
        #self.evtctrlFrame.columnconfigure(0, weight=1)
        self.style.configure("Ctrl.Norm.TButton", width=8)
        self.evtaddButton = Button(self.evtFrame, command=self.add_evt, style="Ctrl.Norm.TButton", text="Add")
        self.evtaddButton.grid(row=3, column=1, padx=5, pady=5, sticky=E)
        self.evteditButton = Button(self.evtFrame, command=self.edit_evt, style="Ctrl.Norm.TButton", text="Edit")
        self.evteditButton.grid(row=3, column=2, padx=5, pady=5)
        self.evtdelButton = Button(self.evtFrame, command=self.del_evt, style="Ctrl.Norm.TButton", text="Delete")
        self.evtdelButton.grid(row=3, column=3, padx=5, pady=5, sticky=W)



        self.hideButton = Button(self.mainFrame, command=self.withdraw, style="Norm.TButton", text="Hide Application")
        self.hideButton.grid(row=1, column=0, columnspan=3, padx=5, pady=5, sticky=EW)

    def codeCommand(self, event=None):
        #messagebox.showinfo("Test", self.testButton.grid_info())
        #messagebox.showinfo("Test", self.weekheaderLabels[3].winfo_width())
        exec(self.codeString.get())
        self.codeEntry.delete(0, END)

    def prevnavButtonCommand(self): #Names
        if self.caldate.month != 1:
            self.caldate = self.caldate.replace(month=self.caldate.month - 1)
        else:
            self.caldate = self.caldate.replace(year=self.caldate.year - 1, month=12)
            #self.seldate.replace(year=3)
        #print(self.seldate)
        #self.month -= 1
        #if self.month == 0:
        #    self.month = 12
        #    self.year -= 1
        self.change_calendar()

    def nextnavButtonCommand(self): #Names
        if self.caldate.month != 12:
            self.caldate = self.caldate.replace(month=self.caldate.month + 1)
        else:
            self.caldate = self.caldate.replace(year=self.caldate.year + 1, month=1)
            #self.seldate.replace(year=6)
        #print(self.seldate)
        #self.month += 1
        #if self.month == 13:
        #    self.month = 1
        #    self.year += 1
        self.change_calendar()

    def change_calendar(self):
        self.mthandyrLabel.configure(text=self.caldate.strftime("%B %Y"))
        weeks = calendar.monthcalendar(self.caldate.year, self.caldate.month)
        while len(weeks) < 6:
            weeks.append([0] * 7)
        for r in range(6):
            for c in range(7):
                if weeks[r][c] != 0:
                    def configure_dayButton(r, c):
                        self.dayandbldayButtons[(r, c)].configure(command=lambda: self.dayButtonCommand(r, c, weeks[r][c]), style="Day.TButton", takefocus=True, text=str(weeks[r][c]))
                    configure_dayButton(r, c)
                else:
                    self.dayandbldayButtons[(r, c)].configure(command=None, style="Blank.Day.TButton", takefocus=False, text="")

    def dayButtonCommand(self, r, c, day): #Names
        self.dayandbldayButtons[self.seldaybuttonpos].configure(style="Day.TButton")
        self.seldaybuttonpos = (r, c)
        self.dayandbldayButtons[(r, c)].configure(style="Sel.Day.TButton") #####disable button?
        self.seldate = self.caldate.replace(day=day)
        self.seldateLabel.configure(text=self.seldate.strftime("%A, %d %B, %Y"))
        if self.evtiids:
            self.evtTreeview.delete(*self.evtiids)
            self.evtiids = []
        self.selevtiid = "" #######delete entrys?
        try:
            for i in range(len(self.data[self.seldate])):
                self.evtiids.append(self.evtTreeview.insert("", END, tags="event", values=self.data[self.seldate][i]))
        except KeyError:
            pass

    def clear_entries(self): #working?
            self.timeEntry.delete(0, END)
            self.evtEntry.delete(0, END)
            self.almEntry.delete(0, END)

    def sel_evt(self, e):
        if self.selevtiid:
            self.evtTreeview.item(self.selevtiid, tags="event")
        if self.selevtiid != self.evtTreeview.identify_row(e.y):
            self.selevtiid = self.evtTreeview.identify_row(e.y)
            self.evtTreeview.item(self.selevtiid, tags="sel event")
            values = self.evtTreeview.item(self.selevtiid, "values")
            self.clear_entries()
            self.timeEntry.insert(0, values[0])
            self.evtEntry.insert(0, values[1])
            self.almEntry.insert(0, values[2])
        else:
            self.selevtiid = ""
            self.clear_entries()

    def add_evt(self):
        if len(self.evtTreeview.get_children()) == 11:
            Message("error", "Only ten events are allowed.")
            return
        time = self.timeEntry.get()
        evt = self.evtEntry.get()
        alm = self.almEntry.get()
        if not time:
            Message("error", "Time cannot be blank.")
            return
        if not evt:
            Message("error", "Event cannot be blank.")
            return
        if not alm:
            Message("error", "Alarm cannot be blank.")
            return
        try:
            self.data[self.seldate].append((time, evt, alm))
        except KeyError:
            self.data[self.seldate] = [(time, evt, alm)]
        with open("time_manager_data", "wb") as file:
            pickle.dump(self.data, file)
        self.evtiids.append(self.evtTreeview.insert("", END, tags="event", values=(time, evt, alm)))
        self.clear_entries()

    def edit_evt(self):
        if not self.selevtiid:
            Message("error", "No event is selected.")
            return
        time = self.timeEntry.get()
        evt = self.evtEntry.get()
        alm = self.almEntry.get()
        if not time:
            Message("error", "Time cannot be blank.")
            return
        if not evt:
            Message("error", "Event cannot be blank.")
            return
        if not alm:
            Message("error", "Alarm cannot be blank.")
            return
        self.data[self.seldate][self.data[self.seldate].index(self.evtTreeview.item(self.selevtiid, "values"))] = (time, evt, alm)
        with open("time_manager_data", "wb") as file:
            pickle.dump(self.data, file)
        self.evtTreeview.item(self.selevtiid, values=(time, evt, alm))
        self.clear_entries()

    def del_evt(self):
        if not self.selevtiid:
            Message("error", "No event is selected.")
            return
        self.data[self.seldate].remove(self.evtTreeview.item(self.selevtiid, "values"))
        with open("time_manager_data", "wb") as file:
            pickle.dump(self.data, file)
        self.evtTreeview.delete(self.selevtiid)
        self.evtiids.remove(self.selevtiid)
        self.selevtiid = ""
        self.clear_entries()

    #def hide_app(self): #####################################NOT THIS
    #    self.withdraw()
    #    def a():
    #        print("a")
    #        Message(self, "error", "Message")
    #    def b():
    #        print("b")
    #        self.deiconify()
    #    self.after(2000, a)
    #    self.after(4000, b)

    ######################################################Check Alarm

    #test_evts = [("12:30", "12 30am evt", "Yes"), ("19:06", "Testing", "Yes"), ("19:18", "Testing2", "Yes"), ("17:00", "5pm evt", "Yes")]

    def check_alarm(self):
        curdt = datetime.datetime.today()
        if curdt.second == 0:
            self.after(60000, self.check_alarm)
            try:
                for evt in self.data[curdt.date()]:
                    if evt[0] == curdt.strftime("%H:%M") and evt[2] == "Yes":
                        #self.focus_force() ###############Not wkring!
                        #messagebox.showinfo("Alarm", "Time: " +  curdt.strftime("%H:%M, %A, %d %B, %Y") + "\nEvent: " + evt[1])
                        Message("alarm", (curdt.strftime("%H:%M, %A, %d %B, %Y"), evt[1]), self) #need self.alarm = Message, for detecting stop sound?, also needs MASTER
                        #alarm.mainloop()
                        #alarm = Toplevel()
            except KeyError:
                pass

        else:
            self.after(1000, self.check_alarm)
        #print("Check Alarm | Current Datetime:", curdt.isoformat())
##        try:
##            for evt in self.data[curdt.date()]:
##                if evt[0] == curdt.strftime("%H:%M") and evt[2] == "Yes":
##                    #self.focus_force() ###############Not wkring!
##                    #messagebox.showinfo("Alarm", "Time: " +  curdt.strftime("%H:%M, %A, %d %B, %Y") + "\nEvent: " + evt[1])
##                    alarm = Message("alarm", curdt.strftime("%H:%M, %A, %d %B, %Y"), evt[1])
##                    #alarm = Toplevel()
##        except KeyError:
##            pass

class Message(Toplevel):
    """..."""

    def __init__(self, messtype, mess, master=None):
        """..."""

        super().__init__(master)
        #self.title("Error")
        #self.configure(background="white", padx=10, pady=10) #change to mainFrame.
        self.attributes("-topmost", True)
        self.resizable(False, False)
        #self.style = Style()
        #self.style.theme_use("alt")
        #self.cou12Font = font.Font(family="Courier", size=12)
        #self.style.configure("Mess.TLabel", background="white", font=self.cou12Font)
        #self.style.configure("Mess.TButton", background="white")
        #self.style.map("TButton"
        #self.style.configure("Mess.TButton", background="white", font=self.cou12Font)
        #self.style.map("Mess.TButton", background=[("pressed", "red3"), ("active", "palevioletred1")])
        self.mainFrame = Frame(self, padding=20, style="Container.TFrame")
        self.mainFrame.pack()
        if messtype == "error":
            self.title("Error")
            #self.mainFrame = Frame(self, padding=10, style="Container.TFrame")
            #self.mainFrame.pack()
            self.messLabel = Label(self.mainFrame, style="Mess.TLabel", text=mess)
            self.messLabel.grid(row=0, column=0, padx=5, pady=5)
            self.okButton = Button(self.mainFrame, style="Norm.TButton", command=self.destroy, text="OK")
            self.okButton.grid(row=1, column=0, padx=5, pady=5)
        if messtype == "alarm": #WHy is it vista theme???##############
            self.title("Alarm")
            self.timeLabel = Label(self.mainFrame, style="Mess.TLabel", text="Time: " + mess[0]) #change to curdt and evt
            self.timeLabel.grid(row=0, column=0, columnspan=2, padx=5, pady=5)
            self.evtLabel = Label(self.mainFrame, style="Mess.TLabel", text="Event: " + mess[1]) #change to curdt and evt
            self.evtLabel.grid(row=1, column=0, columnspan=2, padx=5, pady=5, sticky=EW)
            self.clsButton = Button(self.mainFrame, style="Norm.TButton", command=self.destroy, text="Close")
            self.clsButton.grid(row=2, column=0, padx=5, pady=(20, 5))
            self.showButton = Button(self.mainFrame, style="Norm.TButton", command=master.deiconify, text="Show Application")
            self.showButton.grid(row=2, column=1, padx=5, pady=(20, 5))
            self.alarm()

    def alarm(self):
        self.bell()
        self.after(500, self.alarm)

with open("time_manager_data", "rb") as file:
    data = pickle.load(file)
try:
    if data["running"]:
        app = TimeManager(True)
    else:
        data["running"] = True
        with open("time_manager_data", "wb") as file:
            pickle.dump(data, file)
        app = TimeManager(False, data)
except KeyError:
    data["running"] = True
    with open("time_manager_data", "wb") as file:
        pickle.dump(data, file)
    app = TimeManager(False, data)
#print(data)
app.mainloop()
